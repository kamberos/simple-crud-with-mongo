﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;

namespace NBD_ConnectToMongo
{
    static public class ConnectionSettings
    {
            static public string ConnMongo { get; set; } =
            ConfigurationManager.ConnectionStrings["MongoConnection"].ToString();

    }
}
