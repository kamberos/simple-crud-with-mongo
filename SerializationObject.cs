﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Configuration;


namespace NBD_ConnectToMongo
{
    public class Product
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        
        public Product( string name, double price, string descritpion)
        { 
            Name = name;
            Price = price;
            Description = descritpion;
        }
    }

}
