﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Bson;

namespace NBD_ConnectToMongo
{
    class CRUDOperation
    {
        private readonly IMongoCollection<Product> _product;

        public CRUDOperation()
        {
            var client = new MongoClient(ConnectionSettings.ConnMongo);
            var db = client.GetDatabase("shop");

            _product = db.GetCollection<Product>("products");
        }
        public void GetProduct()
        {
            List<Product> list = _product.AsQueryable().ToList();
            foreach (var item in list)
            {
                Console.WriteLine(item.ToJson());
            }
        }
        public void AddProduct()
        {
            Console.WriteLine("Name of product: ");
            string name = Console.ReadLine();

            Console.WriteLine("Price of product: ");
            double price = Convert.ToDouble(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine("Description of product: ");
            string description = Console.ReadLine();

            var prod = new Product(name, price, description);
            _product.InsertOne(prod);

            Console.WriteLine("{0} hass been added", name);

        }
        public void UpdateProduct()
        {
            Console.WriteLine("Write name of product to update:");
            string name = Console.ReadLine();

            Console.WriteLine("Write new name:");
            string newName = Console.ReadLine();

            Console.WriteLine("Write new price:");
            double price = Convert.ToDouble(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine("Write new description:");
            string description = Console.ReadLine();
            
            var filter = Builders<Product>.Filter.Eq("Name", name);
            var doc = _product.Find(filter).FirstOrDefault();

            if (doc != null)
            {
                var update = Builders<Product>.Update.Set("Name", newName)
                                                     .Set("Price", price)
                                                     .Set("Description", description);
                _product.UpdateOne(filter, update);
                Console.WriteLine("Record {0}, has been update", name);
            }
            else
            {
                Console.WriteLine("Record doesn't exists in DB!");
            }
        }
        public void RemoveProduct()
        {
            Console.WriteLine("Write name of product to remove from DB:");
            string name = Console.ReadLine();
            var filter = Builders<Product>.Filter.Eq("Name", name);

            var doc = _product.Find(filter).FirstOrDefault();
            if (doc != null)
            {
                Console.WriteLine("{0} has been removed", name);
                _product.FindOneAndDelete(x => x.Name == name);
            }
            else
            {
                Console.WriteLine("Record doesn't exists in DB!");
            }
        }
        public void FilterByPrice()
        {
            Console.WriteLine("Write name of product to display:");
            string name = Console.ReadLine();

            var filter = Builders<Product>.Filter.Eq("Name", name);

            var doc = _product.Find(filter).FirstOrDefault();
            if (doc != null)
            {
                var result = _product.FindOneAndDelete(x => x.Name == name);

                Console.WriteLine(result.ToJson());
            }
            else
            {
                Console.WriteLine("Record doesn't exists in DB!");
            }

        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            CRUDOperation cr1 = new CRUDOperation();

            while (true)
            {
                Console.WriteLine("Wybierz operację: \n1 - Dodaj \n2 - Wyswietl \n3 - Aktualizuj \n4 - Usun \n5 - Szukaj\n0 - Wyjdz");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "0":
                        System.Environment.Exit(1);
                        break;
                    case "1":
                        cr1.AddProduct();
                        break;
                    case "2":
                        cr1.GetProduct();
                        break;
                    case "3":
                        cr1.UpdateProduct();
                        break;
                    case "4":
                        cr1.RemoveProduct();
                        break;
                    case "5":
                        cr1.FilterByPrice();
                        break;
                    default:
                        Console.WriteLine("Nieznana operacja!");
                        break;
                }
            }

        }
    }
}

